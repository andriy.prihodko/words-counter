### Setup

    cd server && npm install 
    cd client && npm install

### Run

    cd server && npm start
    cd client && npm start

### Test

    cd server && npm test
    cd client && npm test

Enjoy
