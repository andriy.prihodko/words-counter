export const WordTypes = {
  noun: 'noun',
  verb: 'verb',
  adjective: 'adjective',
  adverb: 'adverb',
  preposition: 'preposition',
  conjunction: 'conjunction',
  pronoun: 'pronoun',
  interjection: 'interjection',
  determiner: 'determiner',
  numeral: 'numeral',
}

export type WordType = (typeof WordTypes)[keyof typeof WordTypes]

export interface WordsCountByType {
  [wordType: WordType]: number
}
