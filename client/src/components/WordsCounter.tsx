import React, { useState } from 'react'
import axios from 'axios'
import { WordsCountByType } from '../interfaces'

const WordsCounter: React.FC = () => {
  const [text, setText] = useState<string>('')
  const [response, setResponse] = useState<WordsCountByType>({})
  const [error, setError] = useState<string | null>(null)

  const handleInputChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value)
  }

  const handleSubmit = async () => {
    try {
      const response = await axios.post<WordsCountByType>('http://localhost:5000/api/v1/words/calculate', { text })
      setResponse(response.data)
      setError(null)
    } catch (err: any) {
      setError(err.message)
      setResponse({})
    }
  }

  return (
    <div>
      <h1>Words Counter</h1>
      <label htmlFor="text-input">Input Text</label>
      <textarea
        id="text-input"
        placeholder="Enter text..."
        value={text}
        onChange={handleInputChange}
        rows={5}
        cols={50}
      />

      {error && <p style={{ color: 'red' }}>{error}</p>}

      {Object.keys(response).length > 0 && (
        <div>
          <p>Results</p>
          <p className="response">
            {Object.entries(response).map(([wordType, count]) => (
              <span key={wordType}>
                {wordType}: {count}
              </span>
            ))}
          </p>
        </div>
      )}

      <div className="actions">
        <button onClick={handleSubmit}>Submit</button>
      </div>
    </div>
  )
}

export default WordsCounter
