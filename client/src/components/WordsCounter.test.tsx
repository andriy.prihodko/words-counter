import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import axios from 'axios'
import WordsCounter from '../../src/components/WordsCounter'

jest.mock('axios')

describe('WordsCounter component', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('renders without errors', () => {
    render(<WordsCounter />)

    const textarea = screen.getByPlaceholderText('Enter text...')
    const button = screen.getByText('Submit')

    expect(screen.getByText('Words Counter')).toBeInTheDocument()
    expect(textarea).toBeInTheDocument()
    expect(button).toBeInTheDocument()
  })

  it('handles input change', () => {
    render(<WordsCounter />)

    const textarea = screen.getByPlaceholderText('Enter text...') as HTMLInputElement

    fireEvent.change(textarea, { target: { value: 'Cat sit' } })

    expect(textarea.value).toBe('Cat sit')
  })

  it('fetches word counts and displays them', async () => {
    const mockedAxios = axios as jest.Mocked<typeof axios>
    mockedAxios.post.mockResolvedValue({
      data: { noun: 2, verb: 1 },
    })

    render(<WordsCounter />)
    const textarea = screen.getByPlaceholderText('Enter text...') as HTMLInputElement
    const button = screen.getByText('Submit')

    fireEvent.change(textarea, { target: { value: 'Two nouns and one verb' } })
    fireEvent.click(button)

    // Wait for the response to be displayed
    const nounCount = await screen.findByText('noun: 2')
    const verbCount = await screen.findByText('verb: 1')

    expect(nounCount).toBeInTheDocument()
    expect(verbCount).toBeInTheDocument()
  })
})
