import React from 'react'
import './App.css'
import WordsCounter from './components/WordsCounter'

function App() {
  return (
    <div className="app">
      <WordsCounter />
    </div>
  )
}

export default App
