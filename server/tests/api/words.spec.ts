import { APIGatewayProxyEvent } from 'aws-lambda'

import { wordsPostCalculate } from '../../src/api/words/post-calculate/handler'
import { ApiResponse, WordTypes } from '../../src/api/words/post-calculate/model'

describe('Words API', () => {
  describe('post words calculate handler', () => {
    describe('when words found in text provided', () => {
      test('should return words counters grouped by type', async () => {
        const testEvent = {
          body: 'cat silently sleep on computer for three hours',
          requestContext: {},
        } as unknown as APIGatewayProxyEvent

        // WHEN request is executed
        const response = await wordsPostCalculate(testEvent)

        // THEN result is expected
        const parsedResultData = JSON.parse(response.body)
        const apiResponse = parsedResultData as ApiResponse

        const expected: ApiResponse = {
          [WordTypes.adjective]: 0,
          [WordTypes.adverb]: 1,
          [WordTypes.conjunction]: 1,
          [WordTypes.determiner]: 0,
          [WordTypes.interjection]: 0,
          [WordTypes.noun]: 2,
          [WordTypes.numeral]: 1,
          [WordTypes.verb]: 1,
          [WordTypes.preposition]: 1,
          [WordTypes.pronoun]: 0,
        }

        expect(response.statusCode).toBe(200)
        expect(apiResponse).toStrictEqual(expected)
      })
    })

    describe('when words not found in text provided', () => {
      test('should return words counters with 0 grouped by type', async () => {
        const testEvent = {
          body: 'bad text',
          requestContext: {},
        } as unknown as APIGatewayProxyEvent

        // WHEN request is executed
        const response = await wordsPostCalculate(testEvent)

        // THEN result is expected
        const parsedResultData = JSON.parse(response.body)
        const apiResponse = parsedResultData as ApiResponse

        const expected: ApiResponse = {
          [WordTypes.adjective]: 0,
          [WordTypes.adverb]: 0,
          [WordTypes.conjunction]: 0,
          [WordTypes.determiner]: 0,
          [WordTypes.interjection]: 0,
          [WordTypes.noun]: 0,
          [WordTypes.numeral]: 0,
          [WordTypes.verb]: 0,
          [WordTypes.preposition]: 0,
          [WordTypes.pronoun]: 0,
        }

        expect(apiResponse).toStrictEqual(expected)
      })
    })

    describe('when provided text is blank', () => {
      test('should respond with error', async () => {
        const testEvent = {
          body: '',
          requestContext: {},
        } as unknown as APIGatewayProxyEvent

        // WHEN request is executed
        const response = await wordsPostCalculate(testEvent)

        // THEN result is expected
        const parsedResultData = JSON.parse(response.body)
        const apiResponse = parsedResultData as ApiResponse

        const expected = {
          error: 'No text provided',
        }

        expect(apiResponse).toStrictEqual(expected)
      })
    })
  })
})
