import { APIGatewayProxyResult } from 'aws-lambda'

import { vocabulary, WordsCountByType } from './model'

export const wordsByType = (text: string): WordsCountByType => {
  const words = text.toLowerCase().match(/\b\w+\b/g) || []
  const result: WordsCountByType = {}

  for (const word of words) {
    for (const wordType in vocabulary) {
      result[wordType] = result[wordType] || 0

      if (vocabulary[wordType].has(word)) {
        result[wordType]++
      }
    }
  }

  return result
}

export const apiResponse = (statusCode: number, response: object): APIGatewayProxyResult => {
  return {
    statusCode,
    body: JSON.stringify(response),
  }
}
