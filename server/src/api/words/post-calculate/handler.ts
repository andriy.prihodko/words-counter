import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'

import { apiResponse, wordsByType } from './helper'
import { ApiResponse } from './model'

export const wordsPostCalculate = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const text = event.body

    if (!text?.length) {
      throw new Error('No text provided')
    }

    const wordsCounts: ApiResponse = wordsByType(text)

    return apiResponse(200, wordsCounts)
  } catch (error) {
    return apiResponse(400, { error: error.message })
  }
}
