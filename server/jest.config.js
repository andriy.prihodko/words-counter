// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html
const data = {
  preset: "ts-jest",
  testEnvironment: "node",
  testTimeout: 15000,
  roots: ["<rootDir>/tests"],
  transform: {
    '^.+\\.tsx?$': ['ts-jest', {
      babel: true,
      tsconfig: "tsconfig.test.json",
    }]
  },
  testRegex: "(.*.spec)\\.ts$",
  moduleFileExtensions: ["ts", "js", "json", "node"],
};

module.exports = data;
